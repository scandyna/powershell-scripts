# PowerShell-scripts

Some helper scripts used in other projects.

# Usage

Here is an example to download and extract the scripts for a given `VERSION`:
```PowerShell
Invoke-WebRequest -Uri "https://gitlab.com/api/v4/projects/39882706/packages/generic/powershell_scripts/$VERSION/powershell_scripts.zip" -OutFile "powershell_scripts.zip" -ErrorAction Stop
Expand-Archive -Path "powershell_scripts.zip" -DestinationPath "." -ErrorAction Stop
```

Note: if taking an archive from the
[package registry](https://gitlab.com/scandyna/powershell-scripts/-/packages),
the link will be different.


## Recommendations

The caller script should set those varaibles:
```PowerShell
# Avoid very slow download times
#
# See:
# - https://stackoverflow.com/questions/28682642/powershell-why-is-using-invoke-webrequest-much-slower-than-a-browser-download
# - https://github.com/PowerShell/PowerShell/issues/2138
# - https://github.com/PowerShell/PowerShell/issues/13414
$ProgressPreference = 'SilentlyContinue'

# On any failure: stop and also fail this script
#
# https://devblogs.microsoft.com/scripting/understanding-non-terminating-errors-in-powershell/
$ErrorActionPreference = 'Stop'
```
