# SPDX-License-Identifier: MIT

$ErrorActionPreference = 'Stop'

Write-Output "Downloading Git ..."

Invoke-WebRequest -Uri "https://github.com/git-for-windows/git/releases/download/v2.46.0.windows.1/Git-2.46.0-64-bit.exe" -OutFile "git_install.exe"
# Invoke-WebRequest -Uri "https://github.com/git-for-windows/git/releases/download/v2.39.2.windows.1/Git-2.39.2-64-bit.exe" -OutFile "git_install.exe"

if(!$?){
  Write-Output "Downloading Git failed"
  exit 1
}

$InstallGitConfigIniFile = "$PSScriptRoot\install_git_config.ini"
Write-Output " git install config: $InstallGitConfigIniFile"

Write-Output "Installing Git ..."
Start-Process -FilePath .\git_install.exe -ArgumentList "/VERYSILENT /LOADINF=$InstallGitConfigIniFile" -NoNewWindow -Wait

if(!$?){
  Write-Output "Installing Git failed, code $LASTEXITCODE"
  exit 1
}

Write-Output "Removing git_install.exe"
Remove-Item "git_install.exe"

Write-Output "Removing $InstallGitConfigIniFile"
Remove-Item "$InstallGitConfigIniFile"

Write-Output "Installing Git done"
