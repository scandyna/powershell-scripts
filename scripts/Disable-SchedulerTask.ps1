# SPDX-License-Identifier: MIT

#
# Inspired from Qt COIN:
# https://code.qt.io/cgit/qt/qt5.git/tree/coin
#

<#
.Synopsis
  Disable a scheduler task
.DESCRIPTION
  Disable a scheduler task
.Parameter Task
    Task identifier
#>
function Disable-SchedulerTask {

  param (
    [Parameter(Mandatory=$true)]
    [string]$Task
  )

  Write-Host " disabling $Task from Task Scheduler"
  SCHTASKS /Change /TN "Microsoft\Windows\$Task" /DISABLE
}
