# SPDX-License-Identifier: MIT

#
# Inspired from Qt COIN:
# https://code.qt.io/cgit/qt/qt5.git/tree/coin
#

. "$PSScriptRoot\Add-KeyToWindowsRegistry.ps1"
. "$PSScriptRoot\Disable-SchedulerTask.ps1"

Write-Output "Disabling Windows defender"

Add-KeyToWindowsRegistry -Path "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows Defender" -Name "DisableAntiSpyware" -Type "DWORD" -Value "1"
Add-KeyToWindowsRegistry -Path "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows Defender" -Name "DisableRoutinelyTakingAction" -Type "DWORD" -Value "1"

# Disable 'QueueReporting' - "Windows Error Reporting task to process queued reports."
Disable-SchedulerTask "Windows Error Reporting\QueueReporting"

# Disable WindowsUpdate from Task Scheduler
Disable-SchedulerTask "WindowsUpdate\Scheduled Start"
