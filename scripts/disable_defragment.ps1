# SPDX-License-Identifier: MIT

#
# Inspired from Qt COIN:
# https://code.qt.io/cgit/qt/qt5.git/tree/coin
#

Write-Output "Disabling defragmentation"

try{
  $state = (Get-ScheduledTask -ErrorAction Stop -TaskName "ScheduledDefrag").State
  Write-Host "Scheduled defragmentation task found in state: $state"
  Write-Host " unregistering scheduled defragmentation task"
  Unregister-ScheduledTask -ErrorAction Stop -Confirm:$false -TaskName ScheduledDefrag
  Write-Host "Scheduled Defragmentation task was cancelled"
}catch{
  Write-Host "No scheduled defragmentation task found"
}
