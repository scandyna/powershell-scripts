# SPDX-License-Identifier: MIT

#
# Inspired from Qt COIN:
# https://code.qt.io/cgit/qt/qt5.git/tree/coin
#

. "$PSScriptRoot\Add-KeyToWindowsRegistry.ps1"
. "$PSScriptRoot\Disable-SchedulerTask.ps1"


Write-Output "Disabling automatic disk cleanup"

Add-KeyToWindowsRegistry -Path "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\StorageSense\Parameters\StoragePolicy" -Name "04" -Type "DWORD" -Value "0"

# Maintenance task used by the system to launch a silent auto disk cleanup when running low on free disk space.
Disable-SchedulerTask "DiskCleanup\SilentCleanup"
