# SPDX-License-Identifier: MIT

$ErrorActionPreference = 'Stop'

. $PSScriptRoot\Add-PathToPATH.ps1

# GitLab needs pwsh executable, which is PowerShell version > 5

# https://learn.microsoft.com/en-us/powershell/scripting/install/installing-powershell-on-windows?view=powershell-7.2
# https://powershellexplained.com/2016-10-21-powershell-installing-msi-files/
# SPDX-License-Identifier: MIT

# https://learn.microsoft.com/en-us/windows-server/administration/windows-commands/msiexec
# https://learn.microsoft.com/en-us/windows/win32/msi/command-line-options

Write-Output "Windows versions:"
Get-ComputerInfo | select WindowsProductName, WindowsVersion, OsHardwareAbstractionLayer

Write-Output "PowerShell version we run on:"
$PSVersionTable

Write-Output "Downloading PowerShell ..."
  Invoke-WebRequest -Uri "https://github.com/PowerShell/PowerShell/releases/download/v7.4.5/PowerShell-7.4.5-win-x64.zip" -OutFile "powershell_7.zip" -ErrorAction Stop
# Invoke-WebRequest -Uri "https://github.com/PowerShell/PowerShell/releases/download/v7.2.6/PowerShell-7.2.6-win-x64.zip" -OutFile "powershell_7.zip" -ErrorAction Stop

Write-Output "Installing PowerShell ..."

$InstallPath = "$env:ProgramFiles\PowerShell\7"
Write-Output "Installing to $InstallPath"

Expand-Archive -Path "powershell_7.zip" -DestinationPath "$InstallPath" -Force -ErrorAction Stop
Add-PathToPATH -Path "$InstallPath"

Write-Output "Removing powershell_7.zip"
Remove-Item "powershell_7.zip"

Write-Output "Installing PowerShell done"

# Below variant using msiexec just did not work.
# Also, there are no error messages visible,
# just the GitLab CI terminating, without being able to upload arttifacts,
# so no logs available.
#
# Write-Output "Downloading PowerShell ..."
# Invoke-WebRequest -Uri "https://github.com/PowerShell/PowerShell/releases/download/v7.2.6/PowerShell-7.2.6-win-x64.msi" -OutFile "C:\TEMP\powershell_install.msi" -ErrorAction Stop
#
# # $Installprocess = Start-Process "msiexec.exe" -ArgumentList "/I C:\TEMP\powershell_install.msi /quiet ENABLE_PSREMOTING=1 REGISTER_MANIFEST=1 USE_MU=1 ENABLE_MU=0 ADD_PATH=1" -NoNewWindow -Wait
#
# $installLogFile = "$INSTALL_LOG_DIR\powershell_install_logs.txt"
# Write-Output "install log file: $installLogFile"
#
# Write-Output "Try to write to log file ..."
# Add-Content -Path "$installLogFile" -Value "*** CI script: to check if log file is writable ***" -ErrorAction Stop
#
# Write-Output "Installing PowerShell ..."
#
# $MSIArguments = @(
#     "/I"
#     "C:\TEMP\powershell_install.msi"
#     "/quiet"
#     "/qn"
#     "/norestart"
#     "/L*v"
#     "$installLogFile"
#     "ENABLE_PSREMOTING=1"
#     "REGISTER_MANIFEST=1"
#     "USE_MU=1"
#     "ENABLE_MU=0"
#     "ADD_PATH=1"
# )
# $Installprocess = Start-Process "msiexec.exe" -ArgumentList $MSIArguments -Wait -NoNewWindow -PassThru
#
# if($Installprocess.ExitCode -ne 0){
#   Throw "Installing PowerShell failed, code: $($Installprocess.ExitCode)"
# }
