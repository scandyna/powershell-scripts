# SPDX-License-Identifier: MIT
<#
.Synopsis
  Adds a key to the Windows registry
.DESCRIPTION
  Adds given key to the registry
.Parameter Path
    The path where to add the key
.Parameter Name
    The name of the key
.Parameter Type
    The type of the key value
    Supported type can be get here: https://learn.microsoft.com/en-us/windows-server/administration/windows-commands/reg-add
    Pass the type without the REG_ prefix
.Parameter Value
    The value for the key
#>
function Add-KeyToWindowsRegistry
{
  param(
    [Parameter(Mandatory=$true)]
    [string]$Path,
    [Parameter(Mandatory=$true)]
    [string]$Name,
    [Parameter(Mandatory=$true)]
    [string]$Type,
    [Parameter(Mandatory=$true)]
    [string]$Value
  )

  Write-Host " adding key $Name (type: $Type, value: $Value) to $Path"

  New-Item -Path Registry::"$Path" -Force
  New-ItemProperty -Path Registry::"$Path" -Name "$Name" -Value "$Value" -PropertyType "$Type"
}
