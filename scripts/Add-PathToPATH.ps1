# SPDX-License-Identifier: MIT
<#
.Synopsis
  Adds a Path to the PATH environment variable
.DESCRIPTION
  Adds given Path to the PATH environment variable
  and save the updated PATH to the registry.
  Inspired by https://github.com/PowerShell/PowerShell/blob/master/tools/install-powershell.ps1
.Parameter Path
    The path to add to the PATH
#>
function Add-PathToPATH
{
  param(
    [string] $Path
  )

  [string] $Environment = 'SYSTEM\CurrentControlSet\Control\Session Manager\Environment'
  [Microsoft.Win32.RegistryKey] $Key = [Microsoft.Win32.Registry]::LocalMachine.OpenSubKey($Environment, [Microsoft.Win32.RegistryKeyPermissionCheck]::ReadWriteSubTree)

  # $key is null here if the user was unable to get ReadWriteSubTree access.
  if($null -eq $Key){
    throw (New-Object -TypeName 'System.Security.SecurityException' -ArgumentList "Unable to access the target registry")
  }

  # Get current unexpanded value
  [string] $FromRegistryCurrentUnexpandedValue = $Key.GetValue('PATH', '', [Microsoft.Win32.RegistryValueOptions]::DoNotExpandEnvironmentNames)

  # Keep current PathValueKind if possible/appropriate
  try {
      [Microsoft.Win32.RegistryValueKind] $PathValueKind = $Key.GetValueKind('PATH')
  } catch {
      [Microsoft.Win32.RegistryValueKind] $PathValueKind = [Microsoft.Win32.RegistryValueKind]::ExpandString
  }

  # Evaluate new path for the registry
  $RegistryNewPathValue = [string]::Concat($FromRegistryCurrentUnexpandedValue.TrimEnd([System.IO.Path]::PathSeparator), [System.IO.Path]::PathSeparator, $Path)

  # Upgrade PathValueKind to [Microsoft.Win32.RegistryValueKind]::ExpandString if appropriate
  if( $RegistryNewPathValue.Contains('%') ){
    $PathValueKind = [Microsoft.Win32.RegistryValueKind]::ExpandString
  }

  $Key.SetValue("PATH", $RegistryNewPathValue, $PathValueKind)

  # Add the new path to current session's PATH (could be different than the one from registry)
  [string] $currentSessionPath = $env:PATH
  $newSessionPath = [string]::Concat($currentSessionPath.TrimEnd([System.IO.Path]::PathSeparator), [System.IO.Path]::PathSeparator, $Path)
  $env:PATH = "$newSessionPath"
}
