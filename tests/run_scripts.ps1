# SPDX-License-Identifier: MIT

# Avoid very slow download times
#
# See:
# - https://stackoverflow.com/questions/28682642/powershell-why-is-using-invoke-webrequest-much-slower-than-a-browser-download
# - https://github.com/PowerShell/PowerShell/issues/2138
# - https://github.com/PowerShell/PowerShell/issues/13414
$ProgressPreference = 'SilentlyContinue'

# On any failure: stop and also fail this script
#
# https://devblogs.microsoft.com/scripting/understanding-non-terminating-errors-in-powershell/
# $ErrorActionPreference = 'Stop'
#
# Update 20240829: above does not return a proper error
# See this job that succeeds despite errors:
# https://gitlab.com/scandyna/powershell-scripts/-/jobs/7695379484

try{
  scripts\disable_disk_cleanup.ps1
  scripts\disable_defragment.ps1
  scripts\disable_windefender.ps1
  scripts\install_PowerShell_7.ps1
  scripts\install_git.ps1
}catch{
  Write-Output "$PSItem"
  exit 1
}
