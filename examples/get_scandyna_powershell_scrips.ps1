# SPDX-License-Identifier: MIT

$ErrorActionPreference = 'Stop'

# URL for v0.0.7
$PackageUrl = "https://gitlab.com/api/v4/projects/39882706/packages/generic/powershell_scripts/0.0.7/powershell_scripts.zip"

Write-Output "Getting package from $PackageUrl"
Invoke-WebRequest -Uri "$PackageUrl" -OutFile "powershell_scripts.zip" -ErrorAction Stop

Write-Output "Unzipping"
Expand-Archive -Path "powershell_scripts.zip" -DestinationPath "." -ErrorAction Stop

ls
